﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIEmenyPatrol : MonoBehaviour
{
    public Animator bodyguardAnimator;

    public int PatrolWidth;
    public int PatrolLength;

    private NavAgentTarget target;
    public NavMeshAgent agent;

    private NavMeshPath navMeshPath;
    private GameObject player;

    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        target = GetComponentInChildren<NavAgentTarget>();
        agent = GetComponentInChildren<NavMeshAgent>();
        navMeshPath = new NavMeshPath();
        RandomizPatrolTarget();
    }
	
	void Update ()
    {
        agent.SetDestination(target.transform.position);
        CanSeePlayer();       
    }

    private void CanSeePlayer()
    {
        RaycastHit hit;
        Vector3 direction = player.transform.position - agent.transform.position;
     
        if (Physics.Raycast(agent.transform.position, direction, out hit))
        {
            if(hit.collider.gameObject.tag == "Player")
            {
                target.transform.position = hit.point;
                bodyguardAnimator.SetBool("foundPlayer", true);
            }
        }
    }

    public void RandomizPatrolTarget()
    {
        bodyguardAnimator.SetBool("foundPlayer", false);

        target.transform.position = new Vector3(Random.Range(transform.position.x - PatrolWidth / 2,
            transform.position.x + PatrolWidth / 2), 1, Random.Range(transform.position.z - PatrolLength / 2, transform.position.z + PatrolLength / 2));

        if (agent.CalculatePath(target.transform.position, navMeshPath) == false)
        {
            RandomizPatrolTarget();
        }
    }
 
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(PatrolWidth, 0, PatrolLength));
    }
}
