﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof(DungeonBuilder))]
public class DungeonBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        DungeonBuilder dungeonManager = (DungeonBuilder)target;

        if(GUILayout.Button("Build"))
        {
            dungeonManager.InspectorBuild();
        }
        if (GUILayout.Button("Clear"))
        {
            dungeonManager.InspectorClear();
        }


    }

	
}
