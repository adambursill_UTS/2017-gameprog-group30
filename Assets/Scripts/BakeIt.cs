﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BakeIt : MonoBehaviour
{
    NavMeshSurface navMeshSurface;

	// Use this for initialization
	void Start ()
    {
        navMeshSurface = GetComponent<NavMeshSurface>();      
	}

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.B))
        {
            navMeshSurface.BuildNavMesh();
        }
    }

}
