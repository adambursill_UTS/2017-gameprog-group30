﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartEndBox : MonoBehaviour
{
    private Animator animator;
    public GameObject door;
    private DungeonBuilder dungeonBuilder;
    bool triggerOnce = true;

    // Use this for initialization
    void Start ()
    {
        dungeonBuilder = GameObject.Find("DungeonManager").GetComponent<DungeonBuilder>();
        animator = door.GetComponent<Animator>();
        animator.SetBool("open", true);
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerStay(Collider other)
    {
        
        if (other.tag == "Player" && tag == "EndBox")
        {
            animator.SetBool("open", false);
            if(triggerOnce)
            {
                dungeonBuilder.RebuildDungeon = true;
                triggerOnce = false;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            animator.SetBool("open", false);
            triggerOnce = true;
        }
    }
}
