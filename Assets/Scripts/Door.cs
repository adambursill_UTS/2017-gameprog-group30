﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Door : MonoBehaviour {

    public static bool doorKey;

    public Text doorText;

    private Animator animator;
    public GameObject door;

	// Use this for initialization
	void Start () {
        doorText.enabled = false;
        animator = door.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
        if (doorKey == true)
        {
            doorText.text = "Press 'F' To Open";
        }

	}

    public void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && doorKey == true && Input.GetKeyDown(KeyCode.F))
        {
            doorText.enabled = true;
            doorText.text = "Press 'F' To Open";
            animator.SetBool("open", true);
        }
        else if (other.tag == "Player")
        {
            doorText.enabled = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            doorText.enabled = false;
            animator.SetBool("open", false);
        }
    }
}
