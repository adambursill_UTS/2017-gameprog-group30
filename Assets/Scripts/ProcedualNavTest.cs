﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcedualNavTest : MonoBehaviour
{
    public GameObject floor;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            GameObject.Instantiate(floor, new Vector3(-25, 0, 0), Quaternion.identity);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            GameObject.Instantiate(floor, new Vector3(0, 0, 25), Quaternion.identity);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            GameObject.Instantiate(floor, new Vector3(25, 0, 0), Quaternion.identity);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            GameObject.Instantiate(floor, new Vector3(0, 0, -25), Quaternion.identity);
        }
    }
}
