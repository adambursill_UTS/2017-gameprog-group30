﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CellRole
{
    Void,
    Room,
    Corridor,
    RegionConnector
}
