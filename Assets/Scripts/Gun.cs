﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Gun : MonoBehaviour {

    public RawImage Bullet1;
    public RawImage Bullet2;
    public RawImage Bullet3;
    public RawImage Bullet4;
    public RawImage Bullet5;
    public RawImage Bullet6;

    public float damage = 10f;
    public float range = 100f;

    public int maxAmmo = 6;
    private int currentAmmo;
    public float reloadTime = 2f;
    private bool isReloading = false;

    public Animator animator;

    public AudioSource gunSounds;

    public AudioClip gunShot;
    public AudioClip reloadGun;

    public Camera playerCamera;
    public ParticleSystem muzzleFlash;
    public GameObject impactEffect;

    private void Start()
    {
        gunSounds.clip = gunShot;
        currentAmmo = maxAmmo;
    }

    // Update is called once per frame
    void Update () {

        if (isReloading) return;

        if (currentAmmo <= 0 || Input.GetKeyDown(KeyCode.R) && currentAmmo < 6)
        {
            StartCoroutine(Reload());
            return;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }

    }

    private IEnumerator Reload()
    {
        isReloading = true;

        Debug.Log("Reloading...");

        animator.SetBool("reloading", true);
        gunSounds.clip = reloadGun;
        gunSounds.Play();
        yield return new WaitForSeconds(reloadTime - .25f);
        animator.SetBool("reloading", false);

        yield return new WaitForSeconds(.25f);

        //The 6 bullet images
        Bullet1.enabled = true;
        Bullet2.enabled = true;
        Bullet3.enabled = true;
        Bullet4.enabled = true;
        Bullet5.enabled = true;
        Bullet6.enabled = true;

        currentAmmo = maxAmmo;
        isReloading = false;
    }

    private void Shoot()
    {
        gunSounds.clip = gunShot;
        gunSounds.Play();
        muzzleFlash.Play();

        currentAmmo--;

        //These Bullets relate to images of bullets on the canvas
        if (currentAmmo == 5)
        {
            Bullet6.enabled = false;
        }
        else if (currentAmmo == 4)
        {
            Bullet5.enabled = false;
        }
        else if (currentAmmo == 3)
        {
            Bullet4.enabled = false;
        }
        else if (currentAmmo == 2)
        {
            Bullet3.enabled = false;
        }
        else if (currentAmmo == 1)
        {
            Bullet2.enabled = false;
        }
        else if (currentAmmo == 0)
        {
            Bullet1.enabled = false;
        }

        //Checks if the raycase hits an enemy, and does damage if it does
        RaycastHit hit;
        if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, range))
        {
            //Debug.Log(hit.transform.name);

            Enemy target = hit.transform.GetComponent<Enemy>();

            if (target != null)
            {
                target.TakeDamage(damage);
            }

            GameObject impact = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impact, 2.0f);
        }
    }
}
