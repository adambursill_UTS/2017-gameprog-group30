﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    public float speed = 6.0f;
    public float gravity = -9.8f;

    public float vertVelocity;
    public float jumpGravity = 0.0f;
    public float jumpForce = 1.0f;

    private CharacterController charCont;

	// Use this for initialization
	void Start () {
        charCont = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {           

        if (charCont.isGrounded)
        {
            vertVelocity = -jumpGravity * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                vertVelocity = jumpForce;
            }
        }
        else
        {
            vertVelocity -= jumpGravity * Time.deltaTime;
        }

        float mouseX = Input.GetAxis("Horizontal") * speed;
        float mouseZ = Input.GetAxis("Vertical") * speed;

        Vector3 movement = new Vector3(mouseX, vertVelocity, mouseZ);
        movement = Vector3.ClampMagnitude(movement, speed);

        movement *= Time.deltaTime;
        movement = transform.TransformDirection(movement);
        charCont.Move(movement);
	}
}