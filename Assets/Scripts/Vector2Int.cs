﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public struct Vector2Int
{

    public int x, z;

    public Vector2Int(int x, int z)
    {
        this.x = x;
        this.z = z;
    }

    public Vector2Int(float x, float z)
    {
        this.x = Convert.ToInt32(x);
        this.z = Convert.ToInt32(z);
    }

    public static Vector2Int operator +(Vector2Int a, Vector2Int b)
    {
        a.x += b.x;
        a.z += b.z;
        return a;
    }

    public static bool operator ==(Vector2Int a, Vector2Int b)
    {
        if(a.x == b.x && a.z == b.z)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool operator !=(Vector2Int a, Vector2Int b)
    {
        if (a.x != b.x || a.z != b.z)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return x + " " + z;
    }
}

