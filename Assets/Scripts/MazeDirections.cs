﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class for holding the different map directions
public static class MazeDirections
{
    static Vector2Int north = new Vector2Int(0, 1);
    static Vector2Int northEast = new Vector2Int(1, 1);
    static Vector2Int south = new Vector2Int(0, -1);
    static Vector2Int southEast = new Vector2Int(1, -1);
    static Vector2Int east = new Vector2Int(1, 0);
    static Vector2Int west = new Vector2Int(-1, 0);
    static Vector2Int southWest = new Vector2Int(-1, -1);
    static Vector2Int northWest = new Vector2Int(-1, 1);

    public static List<Vector2Int> NSEW()
    {
        return new List<Vector2Int>
        {
            north, 
            south, 
            east, 
            west
        };
    }

    public static List<Vector2Int> All()
    {
        return new List<Vector2Int>
        {
            north,
            northEast,
            south,
            southEast,
            east,
            west,
            southWest,
            northWest
        };
    }


}
