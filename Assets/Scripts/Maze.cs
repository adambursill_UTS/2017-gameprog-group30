﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maze
{
    DungeonBuilder dungeonBuilder;
    private int mapWidth;
    private int mapLength;
    private float cellSize;
    private GameObject cellWallPrefab;


    public Maze(DungeonBuilder dungeonBuilder, float cellSize, int mapWidth, int mapLength)
    {
        this.dungeonBuilder = dungeonBuilder;
        this.mapLength = mapLength;
        this.mapWidth = mapWidth;
        this.cellSize = cellSize;

        cellWallPrefab = Resources.Load("CellWall") as GameObject;
    }

    public void RemoveDeadEnds(MapCell[,] cells)
    {
        //removes ALL dead ends
        int count = 1;
        while (count > 0)
        {
            count = 0;
            for (int x = 0; x < mapWidth; x++)
            {
                for (int z = 0; z < mapLength; z++)
                {
                    if (cells[x, z].CellRole == CellRole.Corridor && dungeonBuilder.HasThreeVoids(x, z) == true)
                    {
                        count++;
                        dungeonBuilder.ChangeCell(x, z, Color.blue, CellRole.Void);
                    }
                }
            }
        }
    }

    public void FillMapWithMaze(MapCell[,] cells)
    {       
        //Builds the maze corridors
        for (int x = 0; x < mapWidth - 1; x++)
        {
            for (int z = 0; z < mapLength - 1; z++)
            {
                if (dungeonBuilder.SurroundingCellsAreEmpty(new Vector2Int(x, z)))
                {
                    BuildMaze(x, z, cells);
                }
            }
        }
    }

    public IEnumerator StepAsyncBuildMaze(int x, int z, MapCell[,] cells)
    {
        //Track what cells the maze has used 
        List<MapCell> mapCells = new List<MapCell>();

        // Each new section of maze will have a different id used later for connecting them
        dungeonBuilder.RegionID++;

        // Start by adding a cell to the list and changing it to a corridor
        dungeonBuilder.ChangeCell(x, z, Color.red, CellRole.Corridor);
        yield return null;

        mapCells.Add(cells[x, z]);


        while (mapCells.Count != 0)
        {
            // Depending on how you retrieve the cell from the list will produce different looking mazes
            //MapCell cell = mapCells[mapCells.Count - 1]; // Recursive
            MapCell cell = mapCells[UnityEngine.Random.Range(0, mapCells.Count)]; // Prim's

            List<Vector2Int> possableDirections = new List<Vector2Int>();

            // At the cell we are at look in directions, north, south, east, west
            foreach (Vector2Int p1 in MazeDirections.NSEW())
            {
                bool test = true;

                //If a is not valid in that direction dont do anything else
                if (dungeonBuilder.CanBuildAndIsVoid(cell.Position + p1))
                {
                    // In the direction we are looking also look to its diagonals to see if they
                    // are valid
                    Quaternion rotate90 = Quaternion.AngleAxis(90, Vector3.up);
                    Quaternion rotateNeg90 = Quaternion.AngleAxis(-90, Vector3.up);

                    Vector3 ogVect = new Vector3(p1.x, 0, p1.z);
                    Vector3 rotatedVect = rotate90 * ogVect;
                    Vector3 rotatedVectNeg = rotateNeg90 * ogVect;

                    float newPosX = cell.Position.x + p1.x + p1.x + rotatedVect.x;
                    float newPosZ = cell.Position.z + p1.z + p1.z + rotatedVect.z;

                    Vector2Int vector2DiagnalCheck = new Vector2Int(newPosX, newPosZ);

                    float newPosXNeg = cell.Position.x + p1.x + p1.x + rotatedVectNeg.x;
                    float newPosZNeg = cell.Position.z + p1.z + p1.z + rotatedVectNeg.z;

                    Vector2Int vector2DiagnalNegCheck = new Vector2Int(newPosXNeg, newPosZNeg);

                    // If diagonals are not valid cells, then continue
                    if (!dungeonBuilder.CanBuildAndIsVoid(vector2DiagnalCheck) || !dungeonBuilder.CanBuildAndIsVoid(vector2DiagnalNegCheck))
                    {
                        continue;
                    }

                    // Also check in the direction we are looking all of the cells around it, 
                    // as the maze will build with a signle cell gap between everything
                    foreach (Vector2Int p2 in MazeDirections.NSEW())
                    {
                        // If your looking at the cell we are working from continue
                        if (cell.Position + p1 + p2 == cell.Position)
                        {
                            continue;
                        }
                        // If the direction we are looking is not valid the test fales
                        if (!dungeonBuilder.CanBuildAndIsVoid(cell.Position + p1 + p2))
                        {
                            test = false;
                            break;
                        }
                    }
                    // Any directions that pass all the tests will to added to possable maze directions
                    if (test)
                    {
                        possableDirections.Add(p1);
                    }
                }
            }

            // If the cell we are currently looking at has no possable movment directions avalable then remove it from the list
            if (possableDirections.Count == 0)
            {
                mapCells.Remove(cell);
            }
            else
            {
                // Otherwise pick a random possable direction, change that cell to corrador and add it to the lest. Repeat
                Vector2Int dir = possableDirections[UnityEngine.Random.Range(0, possableDirections.Count)];
                dungeonBuilder.ChangeCell(cell.Position.x + dir.x, cell.Position.z + dir.z, Color.red, CellRole.Corridor);
                mapCells.Add(cells[cell.Position.x + dir.x, cell.Position.z + dir.z]);
                yield return null;
            }
        }
    }

    // This function builds a maze out from the input postion given
    private void BuildMaze(int x, int z, MapCell[,] cells)
    {
        //Track what cells the maze has used 
        List<MapCell> mapCells = new List<MapCell>();

        // Each new section of maze will have a different id used later for connecting them
        dungeonBuilder.RegionID++;

        // Start by adding a cell to the list and changing it to a corridor
        dungeonBuilder.ChangeCell(x, z, Color.red, CellRole.Corridor);
        mapCells.Add(cells[x, z]);


        while (mapCells.Count != 0)
        {
            // Depending on how you retrieve the cell from the list will produce different looking mazes
            //MapCell cell = mapCells[mapCells.Count - 1]; // Recursive
            MapCell cell = mapCells[UnityEngine.Random.Range(0, mapCells.Count)]; // Prim's

            List<Vector2Int> possableDirections = new List<Vector2Int>();

            // At the cell we are at look in directions, north, south, east, west
            foreach (Vector2Int p1 in MazeDirections.NSEW())
            {
                bool test = true;

                //If a is not valid in that direction dont do anything else
                if (dungeonBuilder.CanBuildAndIsVoid(cell.Position + p1))
                {
                    // In the direction we are looking also look to its diagonals to see if they
                    // are valid
                    Quaternion rotate90 = Quaternion.AngleAxis(90, Vector3.up);
                    Quaternion rotateNeg90 = Quaternion.AngleAxis(-90, Vector3.up);

                    Vector3 ogVect = new Vector3(p1.x, 0, p1.z);
                    Vector3 rotatedVect = rotate90 * ogVect;
                    Vector3 rotatedVectNeg = rotateNeg90 * ogVect;

                    float newPosX = cell.Position.x + p1.x + p1.x + rotatedVect.x;
                    float newPosZ = cell.Position.z + p1.z + p1.z + rotatedVect.z;

                    Vector2Int vector2DiagnalCheck = new Vector2Int(newPosX, newPosZ);

                    float newPosXNeg = cell.Position.x + p1.x + p1.x + rotatedVectNeg.x;
                    float newPosZNeg = cell.Position.z + p1.z + p1.z + rotatedVectNeg.z;

                    Vector2Int vector2DiagnalNegCheck = new Vector2Int(newPosXNeg, newPosZNeg);

                    // If diagonals are not valid cells, then continue
                    if (!dungeonBuilder.CanBuildAndIsVoid(vector2DiagnalCheck) || !dungeonBuilder.CanBuildAndIsVoid(vector2DiagnalNegCheck))
                    {
                        continue;
                    }

                    // Also check in the direction we are looking all of the cells around it, 
                    // as the maze will build with a signle cell gap between everything
                    foreach (Vector2Int p2 in MazeDirections.NSEW())
                    {
                        // If your looking at the cell we are working from continue
                        if (cell.Position + p1 + p2 == cell.Position)
                        {
                            continue;
                        }
                        // If the direction we are looking is not valid the test fales
                        if (!dungeonBuilder.CanBuildAndIsVoid(cell.Position + p1 + p2))
                        {
                            test = false;
                            break;
                        }
                    }
                    // Any directions that pass all the tests will to added to possable maze directions
                    if (test)
                    {
                        possableDirections.Add(p1);
                    }
                }
            }

            // If the cell we are currently looking at has no possable movment directions avalable then remove it from the list
            if (possableDirections.Count == 0)
            {
                mapCells.Remove(cell);
            }
            else
            {
                // Otherwise pick a random possable direction, change that cell to corrador and add it to the lest. Repeat
                Vector2Int dir = possableDirections[UnityEngine.Random.Range(0, possableDirections.Count)];
                dungeonBuilder.ChangeCell(cell.Position.x + dir.x, cell.Position.z + dir.z, Color.red, CellRole.Corridor);
                mapCells.Add(cells[cell.Position.x + dir.x, cell.Position.z + dir.z]);
            }
        }
    }


    public void BuildWalls(MapCell[,] cells)
    {
        //Builds the walls for the maze
        for (int x = 0; x < mapWidth; x++)
        {
            for (int z = 0; z < mapLength; z++)
            {
                if (cells[x, z].CellRole != CellRole.Void)
                {
                    BuildCellWalls(x, z);
                }
            }
        }
    }

    private void BuildCellWalls(int x, int z)
    {
        GameObject wall;

        if (dungeonBuilder.GetCellRoll(x, (z + 1)) == CellRole.Void)
        {
            wall = GameObject.Instantiate(cellWallPrefab);
            wall.transform.localPosition = new Vector3(dungeonBuilder.transform.position.x + ((x * cellSize) - (mapWidth * cellSize) * 0.5f + (cellSize * 0.5f)), 5.25f,
            dungeonBuilder.transform.position.z + ((z * cellSize) - (mapLength * cellSize) * 0.5f + (cellSize)));
            wall.transform.localScale = new Vector3(cellSize, 10, 0.2f);
            wall.transform.parent = dungeonBuilder.Walls.transform;
        }
        if (dungeonBuilder.GetCellRoll((x + 1), z) == CellRole.Void)
        {
            wall = GameObject.Instantiate(cellWallPrefab);
            wall.transform.localPosition = new Vector3(dungeonBuilder.transform.position.x + ((x * cellSize) - (mapWidth * cellSize) * 0.5f + (cellSize)), 5.25f,
            dungeonBuilder.transform.position.z + ((z * cellSize) - (mapLength * cellSize) * 0.5f + (cellSize * 0.5f)));
            wall.transform.localScale = new Vector3(cellSize, 10, 0.2f);
            wall.transform.rotation = Quaternion.Euler(0, -90, 0);
            wall.transform.parent = dungeonBuilder.Walls.transform;
        }
        if (dungeonBuilder.GetCellRoll(x, (z - 1)) == CellRole.Void)
        {
            wall = GameObject.Instantiate(cellWallPrefab);
            wall.transform.localPosition = new Vector3(dungeonBuilder.transform.position.x + ((x * cellSize) - (mapWidth * cellSize) * 0.5f + (cellSize * 0.5f)), 5.25f,
            dungeonBuilder.transform.position.z + ((z * cellSize) - (mapLength * cellSize) * 0.5f));
            wall.transform.localScale = new Vector3(cellSize, 10, 0.2f);
            wall.transform.parent = dungeonBuilder.Walls.transform;
        }
        if (dungeonBuilder.GetCellRoll((x - 1), z) == CellRole.Void)
        {
            wall = GameObject.Instantiate(cellWallPrefab);
            wall.transform.localPosition = new Vector3(dungeonBuilder.transform.position.x + ((x * cellSize) - (mapWidth * cellSize) * 0.5f), 5.25f,
            dungeonBuilder.transform.position.z + ((z * cellSize) - (mapLength * cellSize) * 0.5f + (cellSize * 0.5f)));
            wall.transform.localScale = new Vector3(cellSize, 10, 0.2f);
            wall.transform.rotation = Quaternion.Euler(0, 90, 0);
            wall.transform.parent = dungeonBuilder.Walls.transform;
        }
    }
}
