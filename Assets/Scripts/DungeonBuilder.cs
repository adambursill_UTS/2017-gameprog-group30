﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using System.Linq;
using System.Collections;

public class DungeonBuilder : MonoBehaviour
{
    public Material corridorMaterial;
    public Material roomMaterial;

    [Header("Dungeon")]
    public int mapWidth;
    public int mapLength;

    [Header("Room")]
    public int roomAttempts;
    public int roomSize;

    [Header("Build")]
    public bool BuildAtRuntime = false;
    public bool StepBuild = false;

    [Header("Cell")]
    public float cellSize;
    public MapCell mapCellPrefab;
    public int paintingsCount;

    [Header("Enemy")]
    public int enemyCount;
    public int patrolZoneSize;

    [Header("Other")]

    private Maze maze;
    private Rooms rooms;
    private Enemies enemies;
    private MapCell[,] cells;
    private Color mapCellColor;
     
    private StartEndBox startBox;
    private StartEndBox endBox;

    public bool RebuildDungeon { set; get; }
    public int RegionID { get; set; }
    public GameObject Paintings { get; set; }
    public GameObject Enemies { get; set; }
    public GameObject Walls { get; set; }
    public GameObject Map { get; set; }


    // Use this for initialization
    void Start()
    {
        maze = new Maze(this, cellSize, mapWidth, mapLength);
        rooms = new Rooms(this, cellSize, mapWidth, mapLength);
        enemies = new Enemies(this, cellSize, mapWidth, mapLength);
        RegionID = 0;
        Cursor.visible = false;

        if (BuildAtRuntime)
        {
            InspectorClear();
            BuildDungeon();
        }
        else if(StepBuild)
        {
            StartCoroutine(StepAsyncBuildDugeon());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(RebuildDungeon)
        {
            StartCoroutine(AsyncBuildDugeon());
            RebuildDungeon = false;
        }
    }

    IEnumerator StepAsyncBuildDugeon()
    {
        
        InspectorClear();

        Map = new GameObject
        {
            name = "Map"
        };
        Walls = new GameObject
        {
            name = "Walls"
        };
        Paintings = new GameObject
        {
            name = "Paintings"
        };
        Enemies = new GameObject
        {
            name = "Enemies"
        };

        cells = new MapCell[mapWidth, mapLength];
        RegionID = 0;
        for (int x = 0; x < mapWidth; x++)
        {
            for (int z = 0; z < mapLength; z++)
            {
                MakeCell(x, z);
            }
        }


        yield return StartCoroutine(rooms.StepAsyncBuildRooms(roomAttempts, roomSize));      
        rooms.AddStartAndEndBoxes();

        //Builds the maze corridors
        for (int x = 0; x < mapWidth - 1; x++)
        {
            for (int z = 0; z < mapLength - 1; z++)
            {
                if (SurroundingCellsAreEmpty(new Vector2Int(x, z)))
                {
                    //BuildMaze(x, z, cells);
                    //yield return StartCoroutine(StepAsyncBuildMaze(x, z, cells));
                    yield return StartCoroutine(maze.StepAsyncBuildMaze(x, z, cells));
                }
            }
        }

        //yield return StartCoroutine(maze.StepAsyncFillMapWithMaze(cells));
        //maze.FillMapWithMaze(cells);

        yield return StartCoroutine(AsyncStepConnectRoomsAndMazes());


        yield return new WaitForSecondsRealtime(10);
        maze.RemoveDeadEnds(cells);
        yield return new WaitForSecondsRealtime(10);
        maze.BuildWalls(cells);
        yield return new WaitForSecondsRealtime(10);
        rooms.PlacePaintingsInRooms(paintingsCount);
        yield return new WaitForSecondsRealtime(10);
        DestoryVoidCells();
        yield return new WaitForSecondsRealtime(10);
        AddMaterialsToRoomsAndCorradors();
        yield return new WaitForSecondsRealtime(10);
        enemies.AddEnemies(cells, enemyCount, patrolZoneSize);
    }

    //This is the main method that builds the dungeon - it calls all other methods in a step-by-step structure to build up the rooms and maze
    private void BuildDungeon()
    {
        Map = new GameObject
        {
            name = "Map"
        };
        Walls = new GameObject
        {
            name = "Walls"
        };
        Paintings = new GameObject
        {
            name = "Paintings"
        };
        Enemies = new GameObject
        {
            name = "Enemies"
        };

        cells = new MapCell[mapWidth, mapLength];

        for (int x = 0; x < mapWidth; x++)
        {
            for (int z = 0; z < mapLength; z++)
            {
                MakeCell(x, z);
            }
        }

        rooms.BuildRooms(roomAttempts, roomSize);

        rooms.AddStartAndEndBoxes();

        maze.FillMapWithMaze(cells);

        ConnectRoomsAndMazes();

        maze.RemoveDeadEnds(cells);

        maze.BuildWalls(cells);

        rooms.PlacePaintingsInRooms(paintingsCount);

        DestoryVoidCells();

        AddMaterialsToRoomsAndCorradors();

        enemies.AddEnemies(cells, enemyCount, patrolZoneSize);
    }

    IEnumerator AsyncBuildDugeon()
    {
        yield return new WaitForSecondsRealtime(1);

        InspectorClear();

        Map = new GameObject
        {
            name = "Map"
        };
        Walls = new GameObject
        {
            name = "Walls"
        };
        Paintings = new GameObject
        {
            name = "Paintings"
        };
        Enemies = new GameObject
        {
            name = "Enemies"
        };

        cells = new MapCell[mapWidth, mapLength];

        for (int x = 0; x < mapWidth; x++)
        {
            for (int z = 0; z < mapLength; z++)
            {
                MakeCell(x, z);
            }
        }

        mapCellColor = cells[0, 0].GetComponent<Renderer>().material.color;

        rooms.BuildRooms(roomAttempts, roomSize);
        rooms.AddStartAndEndBoxes();
        maze.FillMapWithMaze(cells);
        
        ConnectRoomsAndMazes();
        yield return null;
        maze.RemoveDeadEnds(cells);
        yield return null;
        maze.BuildWalls(cells);
        yield return null;
        rooms.PlacePaintingsInRooms(paintingsCount);
        yield return null;
        DestoryVoidCells();
        yield return null;
        AddMaterialsToRoomsAndCorradors();
        yield return null;
        enemies.AddEnemies(cells, enemyCount, patrolZoneSize);
    }

    private void AddMaterialsToRoomsAndCorradors()
    {
        //Changes material of corridor cells
        for (int x = 0; x < mapWidth; x++)
        {
            for (int z = 0; z < mapLength; z++)
            {
                if (cells[x, z].CellRole == CellRole.Corridor)
                {
                    cells[x, z].gameObject.GetComponent<Renderer>().material = corridorMaterial;
                }
                if (cells[x, z].CellRole == CellRole.Room)
                {
                    cells[x, z].gameObject.GetComponent<Renderer>().material = roomMaterial;
                }
            }
        }
    }

    private void DestoryVoidCells()
    {
        for (int x = 0; x < mapWidth; x++)
        {
            for (int z = 0; z < mapLength; z++)
            {
                if (cells[x, z].CellRole == CellRole.Void)
                {
                    DestroyImmediate(cells[x, z].gameObject);
                }
            }
        }
    }

    public CellRole GetCellRoll(int x, int z)
    {
        return cells[x, z].CellRole;
    }

    public bool HasThreeVoids(int x, int z)
    {
        int voidCount = 0;

        if (cells[x, (z + 1)].CellRole == CellRole.Void)
        {
            voidCount++;
        }
        if (cells[(x + 1), z].CellRole == CellRole.Void)
        {
            voidCount++;
        }
        if (cells[x, (z - 1)].CellRole == CellRole.Void)
        {
            voidCount++;
        }
        if (cells[(x - 1), z].CellRole == CellRole.Void)
        {
            voidCount++;
        }

        if (voidCount >= 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool SurroundingCellsAreEmpty(Vector2Int position)
    {
        foreach (Vector2Int v in MazeDirections.All())
        {
            if (!CanBuildAndIsVoid(position + v))
            {
                return false;
            }
        }
        return true;
    }

    public bool CanBuildAndIsVoid(Vector2Int location)
    {
        // Checks in mulple if statments for readability

        if (location.x < 0 || location.x > mapWidth - 1)
        {
            return false;
        }
        if (location.z < 0 || location.z > mapLength - 1)
        {
            return false;
        }
        if (cells[location.x, location.z].CellRole != CellRole.Void)
        {
            return false;
        }

        return true;
    }

    // Creates a mapCell in a grid at grid postion x, z. Relative to DugneonBuilder
    private void MakeCell(int x, int z)
    {
        MapCell mapCell = Instantiate(mapCellPrefab);
        mapCell.CellRole = CellRole.Void;
        mapCell.Position = new Vector2Int(x, z);

        mapCell.transform.localScale = new Vector3(cellSize, 0.5f, cellSize);
        cells[x, z] = mapCell;
        mapCell.name = "Map Cell " + x + ", " + z;
        mapCell.transform.parent = Map.transform;
        mapCell.transform.localPosition = new Vector3(transform.position.x + ((x * cellSize) - (mapWidth * cellSize) * 0.5f + (cellSize * 0.5f)), 0f,
            transform.position.z + ((z * cellSize) - (mapLength * cellSize) * 0.5f + (cellSize * 0.5f)));
        mapCell.gameObject.layer = 10;
    }

    public void InspectorBuild()
    {
        rooms = new Rooms(this, cellSize, mapWidth, mapLength);
        enemies = new Enemies(this, cellSize, mapWidth, mapLength);
        maze = new Maze(this, cellSize, mapWidth, mapLength);
        RegionID = 0;
        InspectorClear();
        BuildDungeon();
    }

    public void InspectorClear()
    {        
        DestroyImmediate(GameObject.Find("Map"));
        DestroyImmediate(GameObject.Find("Walls"));
        DestroyImmediate(GameObject.Find("Paintings"));
        DestroyImmediate(GameObject.Find("Enemies"));
    }

    IEnumerator AsyncStepConnectRoomsAndMazes()
    {
        // Holds the location of the cell and the two possable regions it connects.
        Dictionary<Vector2Int, List<int>> regionConnections = new Dictionary<Vector2Int, List<int>>();

        do
        {
            regionConnections.Clear();
            for (int x = 1; x < mapWidth - 1; x++)
            {
                for (int z = 1; z < mapLength - 1; z++)
                {
                    List<int> regionsTouching = new List<int>();
                    foreach (Vector2Int v in MazeDirections.NSEW())
                    {
                        Vector2 compareDirection = new Vector2(x + v.x, z + v.z);

                        if (cells[(int)compareDirection.x, (int)compareDirection.y].CellRole != CellRole.Void)
                        {
                            regionsTouching.Add(cells[(int)compareDirection.x, (int)compareDirection.y].RegionID);
                        }
                    }
                    if (regionsTouching.Count == 2 && regionsTouching[0] != regionsTouching[1])
                    {
                        regionConnections[new Vector2Int(x, z)] = regionsTouching;
                    }
                }
            }

            // If no more connections are found then it must be all one region
            if (regionConnections.Keys.Count == 0)
            {
                break;
            }

            List<Vector2Int> connections = regionConnections.Keys.ToList();
            // Pick a random connection
            Vector2Int connection = connections[Random.Range(0, connections.Count)];

            List<int> connectedRegions = regionConnections[connection].ToList();

            int region1 = connectedRegions[0];
            int region2 = connectedRegions[1];
            RegionID = region1;
            // Make the selection a corrador and part of the new region
            ChangeCell(connection.x, connection.z, Color.red, CellRole.Corridor);
            cells[connection.x, connection.z].GetComponent<Renderer>().material.color = Color.green;

            // Find any of the region that has just been merged and change it to the other
            // region

            //print("cells[1, 2].RegionID: " + cells[1, 2].RegionID);



            //for (int x = 1; x < mapWidth - 1; x++)
            //{
            //    for (int z = 1; z < mapLength - 1; z++)
            //    {
            //        print("cells[" + x +"," + z + "].RegionID: " + cells[x, z].RegionID);

            //        Renderer rend = cells[x, z].GetComponent<Renderer>();

            //        if (cells[x, z].RegionID == region2)
            //        {
            //            //if (cells[x, z].RegionID != 0 && rend.material.color != mapCellColor)
            //            //{
            //            //    rend.material.color = Color.white;
            //            //    print("region2: " + region2 + " region1: " + region1);
            //            //}
            //            cells[x, z].RegionID = region1;

            //        }
            //        if (cells[x, z].RegionID == region1 && cells[x, z].RegionID != 0 && rend.material.color != mapCellColor)
            //        {
            //            rend.material.color = Color.white;
            //            print("region2: " + region2 + " region1: " + region1);
            //        }

            //    }
                
            //}

            for (int x = 1; x < mapWidth - 1; x++)
            {
                for (int z = 1; z < mapLength - 1; z++)
                {
                    Renderer rend = cells[x, z].GetComponent<Renderer>();
                    if (cells[x, z].RegionID == region2 && cells[x, z].CellRole != CellRole.Void)
                    {
                        cells[x, z].RegionID = region1;
                        rend.material.color = Color.white;
                    }
                    
                }
                yield return null;
            }
            
        } while ((regionConnections.Keys.Count > 0));
    }

    // This function connects all of the rooms with the mazes that have been generated. everything 
    // will have at least one connection. The function loops through and finds all avalable cells
    // between two different regions and then randomly choses one. Then merges those regions and 
    // continues this loop until all regions are merged into one.
    private void ConnectRoomsAndMazes()
    {       

        // Holds the location of the cell and the two possable regions it connects.
        Dictionary<Vector2Int, List<int>> regionConnections = new Dictionary<Vector2Int, List<int>>();

        do
        {
            regionConnections.Clear();
            for (int x = 1; x < mapWidth - 1; x++)
            {
                for (int z = 1; z < mapLength - 1; z++)
                {
                    List<int> regionsTouching = new List<int>();
                    foreach (Vector2Int v in MazeDirections.NSEW())
                    {
                        Vector2 compareDirection = new Vector2(x + v.x, z + v.z);

                        if (cells[(int)compareDirection.x, (int)compareDirection.y].CellRole != CellRole.Void)
                        {
                            regionsTouching.Add(cells[(int)compareDirection.x, (int)compareDirection.y].RegionID);
                        }
                    }
                    if (regionsTouching.Count == 2 && regionsTouching[0] != regionsTouching[1])
                    {
                        regionConnections[new Vector2Int(x, z)] = regionsTouching;
                    }
                }
            }

            // If no more connections are found then it must be all one region
            if (regionConnections.Keys.Count == 0)
            {
                break;
            }
                
            List<Vector2Int> connections = regionConnections.Keys.ToList();
            // Pick a random connection
            Vector2Int connection = connections[Random.Range(0, connections.Count )];

            List<int> connectedRegions = regionConnections[connection].ToList();

            int region1 = connectedRegions[0];
            int region2 = connectedRegions[1];
            RegionID = region1;
            // Make the selection a corrador and part of the new region
            ChangeCell(connection.x, connection.z, Color.red, CellRole.Corridor);

            // Find any of the region that has just been merged and change it to the other
            // region
            for (int x = 1; x < mapWidth - 1; x++)
            {
                for (int z = 1; z < mapLength - 1; z++)
                {
                    if (cells[x, z].RegionID == region2)
                    {
                        cells[x, z].RegionID = region1;
                    }
                }
            }

        } while ((regionConnections.Keys.Count > 0));       
    }

    //Method to change the roll of the cell: Void / Room / Corridor
    public void ChangeCell(int x, int z, Color color, CellRole cellRole)
    {
#if UNITY_EDITOR
        Material material = new Material(cells[x, z].GetComponent<Renderer>().sharedMaterial);
        material.color = color;
        cells[x, z].GetComponent<Renderer>().sharedMaterial = material;
#else
        cells[x, z].GetComponent<Renderer>().material.color = color;       
#endif
        cells[x, z].CellRole = cellRole;
        cells[x, z].RegionID = RegionID;
        cells[x, z].name += " Region " + RegionID;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, new Vector3(mapWidth * cellSize, 0, mapLength * cellSize));
    }
}
