﻿
using UnityEngine;
using UnityEngine.AI;

class Enemies
{
    DungeonBuilder dungeonBuilder;
    private int mapWidth;
    private int mapLength;
    private float cellSize;

    GameObject aIEmenyPatrol;

    public Enemies(DungeonBuilder dungeonBuilder, float cellSize, int mapWidth, int mapLength)
    {
        this.dungeonBuilder = dungeonBuilder;
        this.mapLength = mapLength;
        this.mapWidth = mapWidth;
        this.cellSize = cellSize;

        aIEmenyPatrol = Resources.Load("AI Enemy Patrol") as GameObject;
    }

    public void AddEnemies(MapCell[,] cells, int enemyCount, int patrolZoneSize)
    {
        dungeonBuilder.Map.AddComponent<NavMeshSurface>().BuildNavMesh();

        int spawmEmemiesCount = enemyCount;
        for (int i = 0; i < spawmEmemiesCount; i++)
        {
            float patrolWidth = (Random.Range(1, 3 + patrolZoneSize) * 2 + 1) * cellSize;
            float patrolLength = (Random.Range(1, 3 + patrolZoneSize) * 2 + 1) * cellSize;

            int x = Random.Range(1, mapWidth - 1);
            int z = Random.Range(1, mapLength - 1);

            if (cells[x, z] != null)
            {
                Vector3 position = new Vector3(dungeonBuilder.transform.position.x + ((x * cellSize) - (mapWidth * cellSize) * 0.5f + (cellSize * 0.5f)), 2f,
            dungeonBuilder.transform.position.z + ((z * cellSize) - (mapLength * cellSize) * 0.5f + (cellSize * 0.5f)));

                GameObject aIEmenyPatrolZ = GameObject.Instantiate(aIEmenyPatrol, position, Quaternion.identity);
                aIEmenyPatrolZ.transform.parent = dungeonBuilder.Enemies.transform;
                aIEmenyPatrolZ.transform.localPosition = position;
                aIEmenyPatrol.gameObject.layer = 10;

                AIEmenyPatrol temp = aIEmenyPatrolZ.GetComponent<AIEmenyPatrol>();
                temp.PatrolWidth = (int)patrolWidth;
                temp.PatrolLength = (int)patrolLength;
               
            }
            else
            {
                spawmEmemiesCount++;
            }
        }
    }


}

