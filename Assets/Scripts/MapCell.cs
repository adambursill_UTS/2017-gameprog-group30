﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCell : MonoBehaviour
{
    public int RegionID { get; set; }
    public CellRole CellRole { get; set; }
    public Vector2Int Position { get; set; }

	// Use this for initialization
	void Start ()
    {
        RegionID = 0;
	}

}
