﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorKey : MonoBehaviour {

    private bool showGUI;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void OnTriggerStay(Collider other)
    {
        showGUI = true;

        if (other.tag == "Player" && Input.GetKeyDown(KeyCode.F))
        {
            Door.doorKey = true;
            Destroy(gameObject);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        showGUI = false;
    }

    public void OnGUI()
    {
        if (showGUI == true)
        {
            GUI.Label(new Rect(880, 445, 200, 20), "Press F To Pick Up Key");
        }
    }

}