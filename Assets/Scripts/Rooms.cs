﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rooms
{
    List<Rect> rooms;
    private DungeonBuilder dungeonBuilder;
    private GameObject startBox;
    private GameObject endBox;
    private bool paintingsPlaced = false;
    private List<Vector2Int> paintingPlacements;
    private List<GameObject> paintingPrefabs;
    private float cellSize;
    private int width, length;

    public Rooms(DungeonBuilder dungeonBuilder, float cellSize, int width, int length)
    {
        paintingPlacements = new List<Vector2Int>();

        this.dungeonBuilder = dungeonBuilder;
        rooms = new List<Rect>();
        this.cellSize = cellSize;
        this.width = width;
        this.length = length;

        paintingPrefabs = new List<GameObject>
        {
            Resources.Load("Abstract") as GameObject,
            Resources.Load("MonaLisa") as GameObject,
            Resources.Load("Pearl") as GameObject,
            Resources.Load("Rose") as GameObject,
            Resources.Load("Scream") as GameObject,
            Resources.Load("Starry") as GameObject,
            Resources.Load("Wave") as GameObject,
            Resources.Load("Sunflowers") as GameObject,
            Resources.Load("Gothic") as GameObject,
            Resources.Load("Memory") as GameObject,
            Resources.Load("Supper") as GameObject,
            Resources.Load("Man") as GameObject,
            Resources.Load("Guernica") as GameObject
        };

    }

    public IEnumerator StepAsyncBuildRooms(int roomAttempts, int roomSize)
    {
        rooms.Clear();

        // Creating some borders so the rooms are not created outside of the maps bounds
        Rect border1Check = new Rect(0, 0, 1, length);
        Rect border2Check = new Rect(0, 0, width, 1);
        Rect border3Check = new Rect(0, length - 1, width, 1);
        Rect border4Check = new Rect(width - 1, 0, 1, length);
        rooms.Add(border1Check);
        rooms.Add(border2Check);
        rooms.Add(border3Check);
        rooms.Add(border4Check);

        // Attempt to add rooms
        for (int i = 0; i < roomAttempts; i++)
        {

            // Get a random position and size to line up with the maze,
            // everything will have a one cell seperation
            int roomWidth = Random.Range(1, 3 + roomSize) * 2 + 1;
            int roomLength = Random.Range(1, 3 + roomSize) * 2 + 1;

            int rectX = (int)Random.Range(0f, (width - roomWidth) * 0.5f) * 2 + 1;
            int rectZ = (int)Random.Range(0f, (length - roomLength) * 0.5f) * 2 + 1;

            Rect room = new Rect(rectX, rectZ, roomWidth, roomLength);

            // Check to see if the room is overlapping another already in the list
            bool overlaps = false;
            foreach (Rect r in rooms)
            {
                if (room.Overlaps(r))
                {
                    overlaps = true;
                    break;
                }
            }
            if (!overlaps)
            {
                rooms.Add(room);
            }
        }

        rooms.Remove(border1Check);
        rooms.Remove(border2Check);
        rooms.Remove(border3Check);
        rooms.Remove(border4Check);

        // Show all of the rooms on the map grid
        foreach (Rect r in rooms)
        {
            dungeonBuilder.RegionID++;
            for (int x = (int)r.x; x < r.xMax; x++)
            {
                for (int z = (int)r.y; z < r.yMax; z++)
                {
                    dungeonBuilder.ChangeCell(x, z, Color.blue, CellRole.Room);
                    
                }
            }
            yield return new WaitForSecondsRealtime(0.1f);
        }
    }


    public void BuildRooms(int roomAttempts, int roomSize)
    {
        rooms.Clear();

        // Creating some borders so the rooms are not created outside of the maps bounds
        Rect border1Check = new Rect(0, 0, 1, length);
        Rect border2Check = new Rect(0, 0, width, 1);
        Rect border3Check = new Rect(0, length - 1, width, 1);
        Rect border4Check = new Rect(width - 1, 0, 1, length);
        rooms.Add(border1Check);
        rooms.Add(border2Check);
        rooms.Add(border3Check);
        rooms.Add(border4Check);

        // Attempt to add rooms
        for (int i = 0; i < roomAttempts; i++)
        {

            // Get a random position and size to line up with the maze,
            // everything will have a one cell seperation
            int roomWidth = Random.Range(1, 3 + roomSize) * 2 + 1;
            int roomLength = Random.Range(1, 3 + roomSize) * 2 + 1;

            int rectX = (int)Random.Range(0f, (width - roomWidth) * 0.5f) * 2 + 1;
            int rectZ = (int)Random.Range(0f, (length - roomLength) * 0.5f) * 2 + 1;

            Rect room = new Rect(rectX, rectZ, roomWidth, roomLength);

            // Check to see if the room is overlapping another already in the list
            bool overlaps = false;
            foreach (Rect r in rooms)
            {
                if (room.Overlaps(r))
                {
                    overlaps = true;
                    break;
                }
            }
            if (!overlaps)
            {
                rooms.Add(room);
            }
        }

        rooms.Remove(border1Check);
        rooms.Remove(border2Check);
        rooms.Remove(border3Check);
        rooms.Remove(border4Check);

        // Show all of the rooms on the map grid
        foreach (Rect r in rooms)
        {
            dungeonBuilder.RegionID++;
            for (int x = (int)r.x; x < r.xMax; x++)
            {
                for (int z = (int)r.y; z < r.yMax; z++)
                {
                    dungeonBuilder.ChangeCell(x, z, Color.blue, CellRole.Room);
                }
            }
        }
    }

    public void AddStartAndEndBoxes()
    {

        float startBoxX = rooms[0].center.x;
        float startBoxZ = rooms[0].center.y;

        if (startBox == null)
        {
            startBox = GameObject.Instantiate(Resources.Load("StartEndBox")) as GameObject;
        }
        startBox.transform.parent = GameObject.Find("Map").transform;
        startBox.name = "Start Box";
        startBox.transform.localPosition = new Vector3(dungeonBuilder.transform.position.x + ((startBoxX * cellSize) - (width * cellSize) * 0.5f), 5.7f,
            dungeonBuilder.transform.position.z + ((startBoxZ * cellSize) - (length * cellSize) * 0.5f));

        float endBoxX = rooms[1].center.x;
        float endBoxZ = rooms[1].center.y;

        if (endBox == null)
        {
            endBox = GameObject.Instantiate(Resources.Load("StartEndBox")) as GameObject;
        }
        endBox.transform.parent = GameObject.Find("Map").transform;
        endBox.name = "End Box";
        endBox.tag = "EndBox";
        endBox.transform.localPosition = new Vector3(dungeonBuilder.transform.position.x + ((endBoxX * cellSize) - (width * cellSize) * 0.5f), 5.7f,
            dungeonBuilder.transform.position.z + ((endBoxZ * cellSize) - (length * cellSize) * 0.5f));

        GameObject.FindGameObjectWithTag("Player").transform.localPosition = new Vector3(dungeonBuilder.transform.position.x + ((startBoxX * cellSize) - (width * cellSize) * 0.5f), 0.82f,
            dungeonBuilder.transform.position.z + ((startBoxZ * cellSize) - (length * cellSize) * 0.5f));
    }

    public void PlacePaintingsInRooms(int paintingsCount)
    {
        foreach (Rect r in rooms)
        {
            for (int i = 0; i < UnityEngine.Random.Range(1, paintingsCount + 1); i++)
            {
                paintingsPlaced = false;
                for (int x = (int)r.x; x < r.xMax; x++)
                {
                    for (int z = (int)r.y; z < r.yMax; z++)
                    {
                        if (paintingsPlaced == true) break;
                        int rand = UnityEngine.Random.Range(1, 5);
                        paintingsPlaced = PlacePaintings(x, z, rand);
                    }
                }
            }
            paintingPlacements.Clear();
        }
    }

    private bool PlacePaintings(int x, int z, int direction)
    {
        GameObject painting;

        switch (direction)
        {
            case 1:
                if (dungeonBuilder.GetCellRoll(x, (z + 1)) == CellRole.Void)
                {
                    if (dungeonBuilder.GetCellRoll(x, z) == CellRole.Room)
                    {
                        Vector2Int pos = new Vector2Int(x, z);
                        if (paintingPlacements.Contains(pos)) return false;
                        painting = GameObject.Instantiate(paintingPrefabs[UnityEngine.Random.Range(0, paintingPrefabs.Count)]);
                        painting.transform.localPosition = new Vector3(dungeonBuilder.transform.position.x + ((x * cellSize) - (width * cellSize) * 0.5f + (cellSize * 0.5f)), 3.5f,
                        dungeonBuilder.transform.position.z + ((z * cellSize) - (length * cellSize) * 0.5f + (cellSize) - 0.175f));
                        //painting.transform.localScale = new Vector3(cellSize, 7, 0.1f);
                        painting.transform.parent = dungeonBuilder.Paintings.transform;
                        paintingPlacements.Add(pos);
                        return true;
                    }
                }
                return false;
            case 2:
                if (dungeonBuilder.GetCellRoll((x + 1), z) == CellRole.Void)
                {
                    if (dungeonBuilder.GetCellRoll(x, z) == CellRole.Room)
                    {
                        Vector2Int pos = new Vector2Int(x, z);
                        if (paintingPlacements.Contains(pos)) return false;
                        painting = GameObject.Instantiate(paintingPrefabs[UnityEngine.Random.Range(0, paintingPrefabs.Count)]);
                        painting.transform.localPosition = new Vector3(dungeonBuilder.transform.position.x + ((x * cellSize) - (width * cellSize) * 0.5f + (cellSize) - 0.175f), 3.5f,
                        dungeonBuilder.transform.position.z + ((z * cellSize) - (length * cellSize) * 0.5f + (cellSize * 0.5f)));
                        //painting.transform.localScale = new Vector3(cellSize, 7, 0.1f);
                        painting.transform.rotation = Quaternion.Euler(0, 90, 0);
                        painting.transform.parent = dungeonBuilder.Paintings.transform;
                        paintingPlacements.Add(pos);
                        return true;
                    }
                }
                return false;
            case 3:
                if (dungeonBuilder.GetCellRoll(x, (z - 1)) == CellRole.Void)
                {
                    if (dungeonBuilder.GetCellRoll(x, z) == CellRole.Room)
                    {
                        Vector2Int pos = new Vector2Int(x, z);
                        if (paintingPlacements.Contains(pos)) return false;
                        painting = GameObject.Instantiate(paintingPrefabs[UnityEngine.Random.Range(0, paintingPrefabs.Count)]);
                        painting.transform.localPosition = new Vector3(dungeonBuilder.transform.position.x + ((x * cellSize) - (width * cellSize) * 0.5f + (cellSize * 0.5f)), 3.5f,
                        dungeonBuilder.transform.position.z + ((z * cellSize) - (length * cellSize) * 0.5f) + 0.175f);
                        //painting.transform.localScale = new Vector3(cellSize, 7, 0.1f);
                        painting.transform.rotation = Quaternion.Euler(0, 180, 0);
                        painting.transform.parent = dungeonBuilder.Paintings.transform;
                        paintingPlacements.Add(pos);
                        return true;
                    }
                }
                return false;
            case 4:
                if (dungeonBuilder.GetCellRoll((x - 1), z) == CellRole.Void)
                {
                    if (dungeonBuilder.GetCellRoll(x, z) == CellRole.Room)
                    {
                        Vector2Int pos = new Vector2Int(x, z);
                        if (paintingPlacements.Contains(pos)) return false;
                        painting = GameObject.Instantiate(paintingPrefabs[UnityEngine.Random.Range(0, paintingPrefabs.Count)]);
                        painting.transform.localPosition = new Vector3(dungeonBuilder.transform.position.x + ((x * cellSize) - (width * cellSize) * 0.5f) + 0.175f, 3.5f,
                        dungeonBuilder.transform.position.z + ((z * cellSize) - (length * cellSize) * 0.5f + (cellSize * 0.5f)));
                        //painting.transform.localScale = new Vector3(cellSize, 7, 0.1f);
                        painting.transform.rotation = Quaternion.Euler(0, -90, 0);
                        painting.transform.parent = dungeonBuilder.Paintings.transform;
                        paintingPlacements.Add(pos);
                        return true;
                    }
                }
                return false;
            default: return false;
        }
    }
}
