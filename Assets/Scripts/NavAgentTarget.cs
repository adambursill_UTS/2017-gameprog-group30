﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavAgentTarget : MonoBehaviour
{
    private AIEmenyPatrol aIEmenyPatrol;

	// Use this for initialization
	void Start ()
    {
        aIEmenyPatrol = transform.parent.GetComponent<AIEmenyPatrol>();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        aIEmenyPatrol.RandomizPatrolTarget();
    }
}
