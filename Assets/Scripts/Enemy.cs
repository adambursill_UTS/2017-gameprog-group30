﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float health = 30f;
    public AIEmenyPatrol enemy;

    public GameObject enemyReference;

    public GameObject ragdollPrefab;
    private GameObject ragdollInstance;

    public void TakeDamage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();
        }
    }

    //Kills the enemy when health >=0 by removing the gameobject and replacing it with a ragdoll at the enemies position.
    private void Die()
    {
        Destroy(transform.parent.gameObject);
        ragdollInstance = Instantiate(ragdollPrefab, new Vector3(enemy.agent.transform.position.x, enemy.agent.transform.position.y, enemy.agent.transform.position.z), Quaternion.identity);
        ragdollInstance.name = "BodyGuardRagdoll";
        ragdollInstance.transform.localRotation = Quaternion.Euler(0, transform.localEulerAngles.y, 0);
        Destroy(ragdollInstance, 30f);
    }
}
